const express = require('express');
const mongoose = require('mongoose');

const app = express();

var dbUrl = 'mongodb://localhost/test'

mongoose.connect(dbUrl, {})
.then(() => console.log("database connected on " + dbUrl))
.catch(err => console.log(err))

// app.get('/', function (req, res) {
// 	res.send("Hello")
// })

const user = require("./model/userData");

app.post('/', function (req, res) {
	console.log(req.query);
	if (!req.query.name) {
		res.status(404).send('name is required');
	}else{

	var newUser = new user({
		name: req.query.name,
		age: req.query.age,
		emailID: req.query.emailID
	})
	newUser.save().then(saveUser=>{
		res.send('data save successfully');
	}).catch(err=>{
		res.send(err);
	})
	}
});

app.get('/', function (req, res) {
	console.log(req.query);
	if (req.query.id) query = {_id : req.query.id}
	if (req.query.name) query = {name : req.query.name}
	if (req.query.age) query = {age : req.query.age}
	if (req.query.email) query = {emailID : req.query.email}
		console.log(query)
	user.find(query, function (err, userDetails) {
		if (err) {
			console.log(err)
			res.send(err)
		}else{
			console.log(userDetails)
			res.send(userDetails)
		}
	})
})

app.put('/:id', function (req, res) {
	// console.log(req.params);
	// console.log(req.query);
	var query = {
		'_id' : req.params.id
	}
	var update = {}
	if (req.query.name) {
		update.name = req.query.name
	}if (req.query.age) {
		update.age = req.query.age
	}if (req.query.email) {
		update.emailID = req.query.email
	}
	console.log(update)
	user.update(query, update, function(err, updatedData) {
		if (err) {
			res.send(err)
		}else{
			res.send(updatedData)
		}		

	})
})

app.delete('/:id', function (req, res) {
	user.findByIdAndRemove(req.params.id,function (err, removedUser) {
		if (err) {
			res.send(err)
		}else{
			res.send(removedUser)
		}
	})
})

app.listen(3000 , () => {
	console.log('server is ready');
})